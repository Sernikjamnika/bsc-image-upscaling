from consts import SCALERS_CONFIG, TEST_IMAGES_CONFIG, SCALING_DEGREE

from upy_scaling.tests.execution_time_tester import ScalerExecutionTimeTester
from training.feature_loss import FeatureLoss


if __name__ == "__main__":
    tester = ScalerExecutionTimeTester(
        TEST_IMAGES_CONFIG["different_images_same_resolution"],
        test_num=100,
        scaling_degree=SCALING_DEGREE)
    tester.test_scalers(SCALERS_CONFIG)
    tester.results_to_csv("results_time_various.csv")
