import argparse
import sys

from consts import SCALERS_CONFIG, TEST_IMAGES_CONFIG, SCALING_DEGREE
from upy_scaling.tests.memory_usage_tester import ScalerMemoryUsageTester
from training.feature_loss import FeatureLoss


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Measure memory used by scaling algorithm')
    parser.add_argument('-s', '--scaler_index', type=int, help="Index of the algorithm in SCALERS_CONFIG list in consts.py")
    parser.add_argument('-i', '--image_index', type=int, help="Index of the image in TEST_IMAGES_CONFIG list in consts.py")
    args = parser.parse_args(sys.argv[1:])
    tester = ScalerMemoryUsageTester(
        [TEST_IMAGES_CONFIG["same_image_different_resolutions"][args.image_index]],
        scaling_degree=SCALING_DEGREE
    )
    tester.test_scalers([SCALERS_CONFIG[args.scaler_index]])
    tester.results_to_csv(f"results_memory_{args.scaler_index}_{args.image_index}.csv")
