import numpy as np
from PIL import Image


class Saver:
    """
    Bazowa klasa zapisująca.
    """
    def save(self, image: Image, filename: str, **kwargs):
        """
        Zapisuje zdjęcie do podanej ścieżki.

        Argumenty:
            image: zdjęcie
            
            filename: ścieżka, gdzie zdjęcie ma być zapisane, nazwa definiuje rozszerzenie pliku
        """
        image.save(filename)
