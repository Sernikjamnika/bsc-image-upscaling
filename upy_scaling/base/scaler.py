from typing import Tuple

import numpy as np

from .loader import Loader
from .saver import Saver


class Scaler:
    """
    Interfejs klasy skalującej.

    Atrybuty:
        loader: klasa ładująca zdjęcia typu `Loader`

        saver: klasa zapisująca zdjęcia typu `Saver`
    """
    loader = Loader()
    saver = Saver()

    def upscale(self, image: np.ndarray, shape: Tuple[int], **kwargs) -> np.ndarray:
        """
        Metoda do nadpisywania, realizująca powiększenie zdjęcia.

        Argumenty:
            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`

            shape: wymiary zdjęcia po powiększeniu w formacie :math:`wysokość \\times szerokość`
        
        Zwraca:
            Powiększone zdjęcie. 
        """
        raise NotImplementedError
