import numpy as np
from PIL import Image


class Loader:
    """ 
    Bazowa klasa do ładowania zdjęć.
    """
    def load_image(self, filename: str, **kwargs) -> Image:
        """
        Ładuje zdjęcie z podanej ścieżki.

        Argumenty:
            filename: ścieżka do zdjęcia zapisanego w przestrzeni barw RGB
        Zwraca:
            Zdjęcie zapisane w obiekcie `PIL.Image`
        
        """
        return Image.open(filename)
