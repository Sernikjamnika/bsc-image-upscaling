from typing import Tuple, Optional

from fastai.basic_train import load_learner
from fastai.layers import NormType
from fastai.vision.image import Image, open_image
from fastai.vision.data import imagenet_stats, ImageImageList
import numpy as np
import torch
import torch.nn.functional as F

from .fastai_dl_loader import FastaiDLLoader
from .fastai_dl_saver import FastaiDLSaver
from ..base.scaler import Scaler


class FastaiDLScaler(Scaler):
    """
    Klasa skalująca zdjęcia za pomocą modelu wytrenowanego
    z wykorzystaniem biblioteki fastai. Rozszerza klasę `Scaler`.

    Atrybuty:
        loader: klasa ładująca zdjęcia typu `FastaiDLLoader`

        saver: klasa zapisująca zdjęcia typu `FastaiDLSaver`

        learner: model głęboko uczony zapisany w obiekcie typu `fastai.basic_train.Learner`
    """

    loader = FastaiDLLoader()
    saver = FastaiDLSaver()

    def __init__(self, path: str, filename: str):
        """
        Inicjalizuje `FastaiDLScaler` z learnerem stworzonym na podstawie
        przekazanych ścieżek do plików.

        Argumenty:
            path: ścieżka, pod którą znajduje się plik z zapisanym `Learnerem`

            filename: nazwa pliku z zapisanym `Learnerem`

        """
        self.learner = load_learner(path, filename)

    def upscale(
        self, image: Image, shape: Optional[Tuple[int]] = None, **kwargs
    ) -> torch.Tensor:
        """
        Metoda realizująca powiększenie zdjęcia za pomocą modelu wytrenowanego
        z wykorzystaniem biblioteki fastai.

        Argumenty:
            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`

            shape: wymiary zdjęcia po powiększeniu w formacie :math:`wysokość \\times szerokość`
        
        Zwraca:
            Powiększone zdjęcie. 
        """
        data = (
            ImageImageList.from_folder("", ignore_empty=True)
            .split_none()
            .label_from_func(lambda x: None)
            .transform([], size=shape)
            .databunch(bs=1, no_check=True)
            .normalize(imagenet_stats, do_y=True)
        )
        self.learner.data = data
        _, result, _ = self.learner.predict(image)
        return result
