from typing import Optional

import numpy as np
import torch
from torch.utils.data import DataLoader

from .torch_dl_loader import TorchDLLoader
from .torch_dl_saver import TorchDLSaver
from ..base.scaler import Scaler


class TorchDLScaler(Scaler):
    """
    Klasa skalująca zdjęcia za pomocą modelu wytrenowanego
    z wykorzystaniem biblioteki PyTorch. Rozszerza klasę `Scaler`.

    Atrybuty:
        loader: klasa ładująca zdjęcia typu `TorchDLLoader`

        saver: klasa zapisująca zdjęcia typu `TorchDLSaver`

        model: model głęboko uczony
    """
    loader = TorchDLLoader()
    saver = TorchDLSaver()

    def __init__(
        self, path: str, filename: str, model: Optional[torch.nn.Module] = None, **kwargs
    ):
        """
        Inicjalizuje `TorchDLScaler` z modelem stworzonym na podstawie
        przekazanych ścieżek do plików i ewentualnie klasy modelu.

        Argumenty:
            path: ścieżka, pod którą znajduje się plik z zapisanym `Learnerem`

            filename: nazwa pliku z zapisanym `Learnerem`

            model: klasa modelu, która zostanie wypełniona wagami

        """
        state_dict = self.model = torch.load(
            f"{path}/{filename}", map_location=torch.device("cpu"), encoding="utf-8"
        )
        if model:
            self.model = model()
            self.model.load_state_dict(state_dict, strict=False)
            self.model.eval()
        else:
            self.model = state_dict["model"]

        torch.set_grad_enabled(False)
        self.model.cpu()

    def upscale(self, image: torch.Tensor, **kwargs) -> torch.Tensor:
        """
        Metoda realizująca powiększenie zdjęcia za pomocą modelu wytrenowanego
        z wykorzystaniem biblioteki PyTorch.

        Argumenty:
            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`
        
        Zwraca:
            Powiększone zdjęcie. 
        """
        return self.model(image)
