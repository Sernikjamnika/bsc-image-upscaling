import numpy as np
import torch
from torchvision.utils import save_image
from PIL import Image

from ..base.saver import Saver


class TorchDLSaver(Saver):
    """
    Klasa zapisująca zdjęcia powiększone przez modele głęboko uczone,
    trenowane z wykorzystaniem biblioteki PyTorch. Rozszerzenie klasy `Saver`.
    """

    def save(self, image: torch.Tensor, filename: str, **kwargs):
        """
        Zapisuje zdjęcie do podanej ścieżki.

        Argumenty:
            image: zdjęcie
            
            filename: ścieżka, gdzie zdjęcie ma być zapisane, nazwa definiuje rozszerzenie pliku

            normalized: wartość logiczna mówiąca, czy zdjęcie zostało znormalizowane podczas odczytu
        """
        if kwargs.get("normalized", False):
            image *= 255
        image = image.reshape(image.shape[-3:])
        image = (
            image.detach().numpy().round().clip(0, 255).astype(np.uint8)
        )  # WARNING detach
        image_object = Image.fromarray(image.transpose(1, 2, 0))
        image_object.save(filename)
