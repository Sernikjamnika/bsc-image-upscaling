import numpy as np
from PIL import Image
import torch
from torch.utils.data import Dataset, DataLoader

from ..base.loader import Loader


class TorchDLLoader(Loader):
    """
    Klasa do ładowania zdjęć dla modeli głęboko uczonych, wytrenowanych za 
    pomocą bilioteki PyTorch. Rozszerzenie klasy `Loader`.
    """

    def load_image(self, filename: str, **kwargs) -> torch.Tensor:
        """
        Ładuje zdjęcie z podanej ścieżki.

        Argumenty:
            filename: ścieżka do zdjęcia zapisanego w przestrzeni barw RGB

            normalized: wartość logiczna mówiąca, czy zdjęcie powinno zostać znormalizowane
        Zwraca:
            Zdjęcie zapisane w obiekcie `torch.Tensor`
        
        """
        normalized = kwargs.get("normalized", False)
        data_loader = DataLoader(
            OneImageDataset(filename, normalized),
            batch_size=1,
            shuffle=False,
            pin_memory=False,
            num_workers=1,
        )
        return data_loader.__iter__().__next__()


class OneImageDataset(Dataset):
    """
    Klasa pomocnicza definiująca `Dataset` wykorzystywany przez `TorchDLLoader`.
    """
    def __init__(self, filename, normalized):
        self.filename = filename
        self.normalized = normalized

    def __getitem__(self, index):
        image = Image.open(self.filename)
        image = np.asarray(image)
        if self.normalized:
            image = image / 255.0
        image_transposed = np.ascontiguousarray(image.transpose((2, 0, 1)))
        tensor = torch.from_numpy(image_transposed).float()
        tensor.to("cpu")
        return tensor

    def __len__(self):
        return 1
