import numpy as np
import torch
from torchvision.utils import save_image

from ..base.saver import Saver


class FastaiDLSaver(Saver):
    """
    Klasa zapisująca zdjęcia powiększone przez modele głęboko uczone,
    trenowane z wykorzystaniem biblioteki fastai. Rozszerzenie klasy `Saver`.
    """
    def save(self, image: torch.Tensor, filename: str, **kwargs):
        """
        Zapisuje zdjęcie do podanej ścieżki.

        Argumenty:
            image: zdjęcie
            
            filename: ścieżka, gdzie zdjęcie ma być zapisane, nazwa definiuje rozszerzenie pliku
        """
        save_image(image, filename)
