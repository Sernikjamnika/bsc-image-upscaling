from fastai.vision.data import open_image, Image
import numpy as np
import torch

from ..base.loader import Loader


class FastaiDLLoader(Loader):
    """
    Klasa do ładowania zdjęć dla modeli głęboko uczonych, wytrenowanych za 
    pomocą bilioteki fastai. Rozszerzenie klasy `Loader`.
    """

    def load_image(self, filename: str, **kwargs) -> Image:
        """
        Ładuje zdjęcie z podanej ścieżki.

        Argumenty:
            filename: ścieżka do zdjęcia zapisanego w przestrzeni barw RGB
        Zwraca:
            Zdjęcie zapisane w obiekcie `fastai.vision.data.Image`
        
        """
        return open_image(filename)
