from typing import Tuple

import PIL
from PIL import Image

from ..base.scaler import Scaler


class LanczosScaler(Scaler):
    """
    Klasa umożliwiająca skalowanie algorytmem Lanczos3. Rozszerza klasę `Scaler`.
    """
    
    def upscale(self, image: Image, shape: Tuple[int], **kwargs) -> Image:
        """
        Metoda powiększająca zdjęcie algorytmem Lanczos3.

        Argumenty:
            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`

            shape: wymiary zdjęcia po powiększeniu w formacie :math:`wysokość \\times szerokość` 
        Zwraca:
            Powiększone zdjęcie. 
        """
        return image.resize(shape[::-1], PIL.Image.LANCZOS)
