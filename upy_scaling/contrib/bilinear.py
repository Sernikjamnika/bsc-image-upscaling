from typing import Tuple

import numpy as np

from .contrib_scaler import ContribScaler


class BilinearContribScaler(ContribScaler):
    def upscale(self, image: np.ndarray, shape: Tuple[int]) -> np.ndarray:
        new_height, new_width = shape
        height, width, _ = image.shape
        width_ratio = (width - 1) / new_width
        height_ratio = (height - 1) / new_height

        x = np.full((1, new_width), width_ratio) * np.arange(new_width)
        y = np.full((1, new_height), height_ratio) * np.arange(new_height)

        x0 = np.floor(x).astype(int)
        x1 = x0 + 1
        y0 = np.floor(y).astype(int)
        y1 = y0 + 1

        x0 = np.clip(x0, 0, image.shape[1] - 1)
        x1 = np.clip(x1, 0, image.shape[1] - 1)
        y0 = np.clip(y0, 0, image.shape[0] - 1)
        y1 = np.clip(y1, 0, image.shape[0] - 1)

        x, y = self._create_index_matricies(x, y, new_height, new_width)
        x0, y0 = self._create_index_matricies(x0, y0, new_height, new_width)
        x1, y1 = self._create_index_matricies(x1, y1, new_height, new_width)

        image_a = self._indices_to_image(x0, y0, image)
        image_b = self._indices_to_image(x1, y0, image)
        image_c = self._indices_to_image(x0, y1, image)
        image_d = self._indices_to_image(x1, y1, image)

        weights_a, weights_b, weights_c, weights_d = self.__create_weights(
            x, y, x0, y0, x1, y1
        )

        result = (
            weights_a * image_a
            + weights_b * image_b
            + weights_c * image_c
            + weights_d * image_d
        )

        return result

    def __create_weights(self, x, y, x0, y0, x1, y1):
        weights_a = (x1 - x) * (y1 - y)
        weights_b = (x - x0) * (y1 - y)
        weights_c = (x1 - x) * (y - y0)
        weights_d = (x - x0) * (y - y0)

        weights_a = np.reshape(weights_a, weights_a.shape + (1,))
        weights_b = np.reshape(weights_b, weights_b.shape + (1,))
        weights_c = np.reshape(weights_c, weights_c.shape + (1,))
        weights_d = np.reshape(weights_d, weights_d.shape + (1,))

        return weights_a, weights_b, weights_c, weights_d

    def __str__(self):
        return "Bilinear"
