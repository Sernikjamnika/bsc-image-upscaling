import numpy as np

from typing import List, Tuple

from .contrib_scaler import ContribScaler


class NNContrib(ContribScaler):
    def upscale(self, img: np.ndarray, shape: Tuple[int]) -> np.ndarray:
        new_height, new_width = shape
        height, width, _ = img.shape

        width_ratio = width / new_width
        height_ratio = height / new_height

        x_indices_vector = self._create_indices_vector(new_width, width_ratio)
        y_indices_vector = self._create_indices_vector(new_height, height_ratio)

        x_indices_matrix, y_indices_matrix = self._create_index_matricies(
            x_indices_vector, y_indices_vector, new_height, new_width
        )

        rescaled_img = self._indices_to_image(x_indices_matrix, y_indices_matrix, img)
        return rescaled_img

    def _create_indices_vector(self, dimension_new_size, dimension_ratio):
        indices = np.full((1, dimension_new_size), dimension_ratio)
        indices = np.floor(indices * np.arange(dimension_new_size))
        indices = indices.astype(int)
        indices = np.clip(indices, 0, dimension_new_size - 1)
        return indices

    def __str__(self):
        return "NNContrib"
