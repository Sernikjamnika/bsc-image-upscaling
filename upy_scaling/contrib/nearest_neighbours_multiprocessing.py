import functools
import multiprocessing
from typing import List, Tuple

import numpy as np

from .nearest_neighbours import NNContrib


class NNContribMultiprocess(NNContrib):
    def rescaling(
        self,
        img: np.ndarray,
        rescaled_height: int,
        rescaled_width: int,
        height: int,
        width: int,
        width_ratio: float,
        height_ratio: float,
        num_workers: int,
    ) -> np.ndarray:
        rescaled_img = []
        for cell_height in range(0, rescaled_height // num_workers):
            for cell_width in range(rescaled_width):
                rescaled_cell = self._get_cell(
                    img,
                    height,
                    width,
                    cell_height,
                    cell_width,
                    width_ratio,
                    height_ratio,
                )
                rescaled_img.append(rescaled_cell)
        return rescaled_img

    def upscale(
        self, img: np.ndarray, shape: Tuple[int], num_workers: int = 4
    ) -> np.ndarray:
        rescaled_height, rescaled_width = shape
        height, width, _ = img.shape

        width_ratio = width / rescaled_width
        height_ratio = height / rescaled_height

        rescaling_function = functools.partial(
            self.rescaling,
            rescaled_height=rescaled_height,
            rescaled_width=rescaled_width,
            height=height,
            width=width,
            width_ratio=width_ratio,
            height_ratio=height_ratio,
            num_workers=num_workers,
        )

        # appends to list are faster than to array
        with multiprocessing.Pool(num_workers) as pool:
            rescaled_img = pool.map(rescaling_function, np.split(img, num_workers))

        rescaled_img = np.asarray(rescaled_img)
        rescaled_img = rescaled_img.reshape((rescaled_height, rescaled_width, 3))
        return rescaled_img

    def __str__(self):
        return "NNContribMultiprocess"
