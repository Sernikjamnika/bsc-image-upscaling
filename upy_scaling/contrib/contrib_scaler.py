from typing import Tuple, List

import numpy as np

from .loader import ContribLoader
from .saver import ContribSaver
from ..base.scaler import Scaler


class ContribScaler(Scaler):
    loader = ContribLoader()
    saver = ContribSaver()

    def _indices_to_image(
        self, x: List[List[int]], y: List[List[int]], image: np.ndarray
    ) -> np.ndarray:
        return image[y, x]

    def _create_index_matricies(
        self, x: np.ndarray, y: np.ndarray, new_height: int, new_width: int
    ):
        x = np.tile(x, (new_height, 1))
        y = np.tile(y.T, new_width)
        return x, y
