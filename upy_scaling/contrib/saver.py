import numpy as np
from PIL import Image


class ContribSaver:
    def save(self, image: np.ndarray, filename: str, format="PNG"):
        if image.dtype is not np.uint8:
            image = np.uint8(image)
        img_obj = Image.fromarray(image)
        img_obj.save(filename, format)
