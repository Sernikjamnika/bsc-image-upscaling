import numpy as np
from PIL import Image

from ..base.loader import Loader


class ContribLoader(Loader):
    def load_image(self, filename: str) -> np.ndarray:
        image = super().load_image(filename)
        return np.asarray(image)
