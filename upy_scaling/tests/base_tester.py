from typing import List, Tuple

from PIL import Image

from .report_generators.base_report_generator import ReportGenerator
from ..base.scaler import Scaler

class Tester:
    """
    Bazowa klasa testująca, przeznaczona do nadpisywania.

    Atrybuty:
        report_generator: obiekt generująca raporty typu `ReportGenerator`

        test_num: liczba testów dla pary (skaler, zdjęcie)

        scaling_degree: poziom powiększenia zdjęcia, ze względu na to, że 
        wiele architektur głęboko uczonych zwiększa rozdzielczość o zadany mnożnik

        images_data: lista ścieżek do zdjęć wykorzystywana do testowania
    """
    report_generator = ReportGenerator()

    def __init__(
        self, images_data: List[dict], test_num: int = 100, scaling_degree: int = 4
    ):
        """
        Inicjalizuje `Tester` z zadaną listą zdjęć przeznaczoną do testów i innymi parametrami.

        Argumenty:
            images_data: lista ścieżek do zdjęć wykorzystywana do testowania

            test_num: liczba testów dla pary (skaler, zdjęcie)

            scaling_degree: poziom powiększenia zdjęcia, ze względu na to, że 
            wiele architektur głęboko uczonych zwiększa rozdzielczość o zadany mnożnik
        """
        self.test_num = test_num
        self.scaling_degree = scaling_degree
        self.images_data = images_data

    def test_scalers(self, scalers_config: List[dict]):
        """
        Testuje każdy skaler z każdym zdjęciem poprzez wywołanie metody `test`.

        Argumenty:
            scalers_config: lista słowników z konfiguracją skalerów do testowania
        """
        for scaler_config in scalers_config:
            scaler_name = str(scaler_config["scaler_class"]) + scaler_config.get("filename", "")
            scaler_class = scaler_config.pop("scaler_class")
            scaler = scaler_class(**scaler_config)
            normalized = scaler_config.get("normalized", False) 
            for image_data in self.images_data:
                height, width = image_data["shape"]
                image = scaler.loader.load_image(image_data["filename"], normalized=normalized)
                result = self.test(
                    scaler,
                    image,
                    (height * self.scaling_degree, width * self.scaling_degree)
                )
                self.report_generator.add_to_report(
                    scaler_name, image_data["filename"], width, height, self.scaling_degree, result
                )


    def test(
        self, scaler: Scaler, image: Image, new_shape: Tuple[int]
    ) -> float:
        """
        Metoda do nadpisywania, testująca skaler na zadanym zdjęciu.

        Argumenty:
            scaler: skaler do testowania

            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`

            new_shape: wymiary zdjęcia po powiększeniu w formacie :math:`wysokość \\times szerokość`
        
        Zwraca:
            Wynik testu.
        """
        raise NotImplementedError()

    def results_to_csv(self, filename: str = "results.csv"):
        """
        Zapisuje resultaty testów do pliku csv za pomocą `report_generator`.

        Argumenty:
            filename: ścieżka do pliku, w którym ma zostać zapisany raport z testów
        """
        self.report_generator.to_csv(filename)
