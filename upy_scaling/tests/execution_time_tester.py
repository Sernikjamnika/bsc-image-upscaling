import timeit
from typing import Tuple

from PIL import Image

from upy_scaling.tests.base_tester import Tester
from ..base.scaler import Scaler

class ScalerExecutionTimeTester(Tester):
    """
    Klasa testująca czas wykonania operacji powiększania. Rozszerza `Tester`.

    Atrybuty:
        report_generator: obiekt generująca raporty typu `ReportGenerator`

        test_num: liczba testów dla pary (skaler, zdjęcie)

        scaling_degree: poziom powiększenia zdjęcia, ze względu na to, że 
        wiele architektur głęboko uczonych zwiększa rozdzielczość o zadany mnożnik

        images_data: lista ścieżek do zdjęć wykorzystywana do testowania
    """

    def test(
        self, scaler: Scaler, image: Image, new_shape: Tuple[int]
    ) -> float:
        """
        Metoda testująca czas wykonania przez skaler powiększenia na zadanym zdjęciu
        do danej wielkości.

        Argumenty:
            scaler: skaler do testowania

            image: zdjęcie zapisane w postaci macierzowej :math:`wysokość \\times szerokość \\times liczba\_kanałów`

            new_shape: wymiary zdjęcia po powiększeniu w formacie :math:`wysokość \\times szerokość`
        
        Zwraca:
            Wynik testu.
        """
        scaling_time = timeit.timeit(
            f"scaler.upscale(image, shape={new_shape})",
            number=self.test_num,
            globals=locals(),
        )
        return scaling_time / self.test_num
