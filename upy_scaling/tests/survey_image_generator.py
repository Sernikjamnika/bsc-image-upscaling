import math
import random
from typing import List, Tuple

from PIL import Image, ImageDraw, ImageFont


TRUE_TYPE_FONT_FILE = "/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf"
FONT_SIZE = 28

def generate_survey_image(
    sub_images: List[str],
    sub_image_size: Tuple[int],
    offset: int = 40,
    images_in_row: int = 3,
) -> Tuple[Image, List[str]]:
    """
    Generuje zdjęcia do testów jakościowych poprzez sklejanie zdjęć składowych w siatki. 
    Lista ścieżek do zdjęć jest losowo mieszna. Każde zdjęcie jest numerowane poprzez dodanie pod nim białego tła z liczbą.
    
    Argumenty:
        sub_images: lista ścieżek do zdjęć wchodzących w skład siatki
        sub_image_size: wielkość każdego zdjęcia wchodzącego w skład `sub_images` (muszą być jednakowo wielkie)
        offset: wielkość białego tła (uwaga musi być wystarczająco duże, aby liczba się na nim zmieściła)
        images_in_row: liczba zdjęć w wierszu
    Zwraca:
        Siatkę zdjęć z przypisanymi numerami i listę z kolejnością zdjęć.
    """
    font = ImageFont.truetype(TRUE_TYPE_FONT_FILE, size=FONT_SIZE)
    sub_image_width, sub_image_height = sub_image_size
    generated_image = Image.new(
        "RGB",
        (
            sub_image_width * images_in_row,
            (sub_image_height + offset) * math.ceil(len(sub_images) / images_in_row),
        ),
    )
    random.shuffle(sub_images)
    for index, sub_image in enumerate(sub_images):
        sub_image = Image.open(sub_image)
        sub_image_indexed = Image.new(
            "RGB", (sub_image_width, sub_image_height + offset), color="white"
        )
        sub_image_indexed.paste(sub_image, (0, 0))
        image_draw = ImageDraw.Draw(sub_image_indexed)
        image_draw.text(
            (sub_image_width // 2, sub_image_height + 5),
            str(index),
            font=font,
            fill=(0, 0, 0, 255),
        )
        generated_image.paste(
            sub_image_indexed,
            (
                (index % images_in_row) * sub_image_width,
                (index // images_in_row) * (sub_image_height + offset),
            ),
        )
    return generated_image, sub_images
