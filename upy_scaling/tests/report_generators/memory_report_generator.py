import csv
from operator import attrgetter
from typing import Tuple
import os

from .base_report_generator import ReportGenerator


class MemoryUsageReportGenerator(ReportGenerator):
    """
    Klasa generująca raporty z pomiaru zużytej pamięci w trakcie wykonywania algorytmu.

    Atrybuty:
        results: lista słowników z rezultatami testów
    """
    results = []

    def add_to_report(
        self,
        scaler_name: str,
        image_name: str,
        height: int,
        width: int,
        upscaling_degree: int,
        value: Tuple[float],
    ):
        """
        Dodaje wyniki testu do raportu osobno dla średniej pamięci zużytej i maksymalnej.
        Jeśli dla danego algorytmu nie ma jeszcze danych, tworzony jest odpowiedni klucz w `results`.

        Argumenty:
            scaler_name: nazwa algorymtu skalującego
            
            image_name: nazwa zdjęcia, na którym przeprowadzany byl test

            height: wielkość zdjęcia przed powiększeniem 
            
            width: szerokośc zdjęcia przed powiększeniem

            upscaling_degree: stopień powiększenia w wysokości i szerokości

            value: para złożona odpowiednio ze średniego zużycia i wartości maksymalnej
        """
        scaler_results = self.get_scaler_results(scaler_name)
        column_name = (
            f"{image_name}_{height}_{width}_{height * width}_{upscaling_degree}"
        )
        scaler_results[column_name + "_mean"], scaler_results[column_name + "_max"] = value

    def to_csv(self, filename: str = "results.csv"):
        """
        Eksportuje rezultaty do pliku w formacie CSV poprzez dopisanie ich w nowej linii 
        na końcu pliku. Jeśli plik jest pusty dodawany jest nagłówek z nazwami kolumn.

        Argumenty:
            filename: nazwa pliku docelowego
        """
        field_names = list(self.results[0].keys())
        field_names.sort()
        with open(filename, mode="a") as file:
            writer = csv.DictWriter(file, field_names)
            if os.stat(filename).st_size == 0:
                writer.writeheader()
            writer.writerows(self.results)