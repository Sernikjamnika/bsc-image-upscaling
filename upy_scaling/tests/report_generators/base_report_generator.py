import csv
from operator import attrgetter


class ReportGenerator:
    """
    Bazowa klasa generująca raporty.

    Atrybuty:
        results: lista słowników z rezultatami testów
    """
    results = []

    def add_to_report(
        self,
        scaler_name: str,
        image_name: str,
        height: int,
        width: int,
        upscaling_degree: int,
        value: float,
    ):
        """
        Dodaje wynik do raportu. Jeśli dla danego algorytmu nie ma jeszcze danych, tworzony
        jest odpowiedni klucz w `results`.

        Argumenty:
            scaler_name: nazwa algorymtu skalującego
            
            image_name: nazwa zdjęcia, na którym przeprowadzany byl test

            height: wielkość zdjęcia przed powiększeniem 
            
            width: szerokośc zdjęcia przed powiększeniem

            upscaling_degree: stopień powiększenia w wysokości i szerokości

            value: wartość rezultatu testu
        """
        scaler_results = self.get_scaler_results(scaler_name)
        column_name = (
            f"{image_name}_{height}_{width}_{height * width}_{upscaling_degree}"
        )
        scaler_results[column_name] = value

    def add_scaler_new_results(self, scaler_name: str) -> dict:
        """
        Dodaje nowy klucz do rezultatów testów.

        Argumenty:
            scaler_name: nazwa algorytmu skalującego do stworzenia klucza
        """
        scaler_results = {}
        scaler_results["scaler_name"] = scaler_name
        self.results.append(scaler_results)
        return scaler_results

    def to_csv(self, filename: str = "results.csv"):
        """
        Eksportuje rezultaty do pliku w formacie CSV.

        Argumenty:
            filename: nazwa pliku docelowego
        """
        field_names = list(self.results[0].keys())
        field_names.sort()
        with open(filename, mode="w") as file:
            writer = csv.DictWriter(file, field_names)
            writer.writeheader()
            writer.writerows(self.results)

    def get_scaler_results(self, scaler_name):
        """
        Pobiera rezultaty testów dla danego algorytmu skalującego.

        Argumenty:
            scaler_name: nazwa algorytmu skalującego
        """
        for scaler_results in self.results:
            if scaler_results["scaler_name"] == scaler_name:
                return scaler_results
        return self.add_scaler_new_results(scaler_name)
