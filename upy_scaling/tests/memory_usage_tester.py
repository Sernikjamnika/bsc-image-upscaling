from typing import Tuple, List

import memory_profiler
import numpy as np
from PIL import Image

from upy_scaling.tests.base_tester import Tester
from .report_generators.memory_report_generator import MemoryUsageReportGenerator
from ..base.scaler import Scaler


class ScalerMemoryUsageTester(Tester):
    """
    Klasa testująca wykorzystaną pamięć w trakcie operacji powiększania. Rozszerza `Tester`.

    Atrybuty:
        report_generator: obiekt generująca raporty typu `MemoryUsageReportGenerator`

        test_num: liczba testów dla pary (skaler, zdjęcie)

        scaling_degree: poziom powiększenia zdjęcia, ze względu na to, że 
        wiele architektur głęboko uczonych zwiększa rozdzielczość o zadany mnożnik

        images_data: lista ścieżek do zdjęć wykorzystywana do testowania
    """
    report_generator = MemoryUsageReportGenerator()

    def test_scalers(self, scalers_config: List[dict]):
        """
        Testuje dokładnie jeden skaler, pierwszy z konfiguracji, z każdym zdjęciem poprzez sprawdzanie 
        zajmowanej pamięci przez proces w interwałach równych 0.1 sekundy. Test dla jednego zdjęcia wykonuje się 
        jeden raz niezależnie od atrybutu `test_num`. Ograniczenia i brak nadpisywania metody `test` zostały
        nałożone ze względu na duże zużycie pamięci przez architektury głęboko uczone.

        Argumenty:
            scalers_config: lista słowników z konfiguracją skalerów do testowania
        """
        scaler_config = scalers_config[0]
        scaler_name = str(scaler_config["scaler_class"]) + scaler_config.get("filename", "")
        scaler_class = scaler_config.pop("scaler_class")
        scaler = scaler_class(**scaler_config)
        normalized = scaler_config.get("normalized", False)

        for image_data in self.images_data:
            height, width = image_data["shape"]
            image = scaler.loader.load_image(image_data["filename"], normalized=normalized)
            memory = memory_profiler.memory_usage(
                (
                    scaler.upscale,
                    (image,),
                    {
                        "shape": (
                            self.scaling_degree * height,
                            self.scaling_degree * width,
                        )
                    },
                )
            )
            self.report_generator.add_to_report(
                scaler_name,
                image_data["filename"],
                width,
                height,
                self.scaling_degree,
                (np.mean(memory), np.max(memory))
            )
