from upy_scaling.canonical.bilinear import BilinearScaler
from upy_scaling.canonical.lanczos import LanczosScaler
from upy_scaling.canonical.nearest_neighbours import NearestNeighboursScaler
from upy_scaling.dl.fastai_dl_scaler import FastaiDLScaler
from upy_scaling.dl.torch_dl_scaler import TorchDLScaler
from models.edsr import EDSR
from models.esrgan import RRDBNet


SCALING_DEGREE = 4

SCALERS_CONFIG = [
    {"scaler_class": NearestNeighboursScaler},
    {"scaler_class": BilinearScaler},
    {"scaler_class": LanczosScaler},
    {
        "scaler_class": TorchDLScaler,
        "path": "./saved_models",
        "filename": "rrdb_esrgan_x4_pytorch.pth",
        "model": RRDBNet,
        "normalized": True,
    },
    {
        "scaler_class": TorchDLScaler,
        "path": "./saved_models",
        "filename": "edsr_pytorch.pt",
        "model": EDSR,
    },
    {"scaler_class": FastaiDLScaler, "path": "./saved_models", "filename": "unet_gan_resnet34_fastai_learner"},
    {"scaler_class": FastaiDLScaler, "path": "./saved_models", "filename": "unet_resnet34_fastai_learner"},
    {"scaler_class": FastaiDLScaler, "path": "./saved_models", "filename": "unet_resnext50_32x4d_fastai_learner"},
]

IMAGES_CONFIG = [
    {"filename": "test_images/baboon_125_120.png", "shape": (120, 125), "name": "baboon_125_120"},
    {"filename": "test_images/checkers_99_99.png", "shape": (99, 99), "name": "checkers_99_99"},
    {"filename": "test_images/digits_100_178.png", "shape": (178, 100), "name": "digits_100_178"},
    {"filename": "test_images/dog_150_84.png", "shape": (84, 150), "name": "dog_150_84"},
    {"filename": "test_images/house_200_132.png", "shape": (132, 200), "name": "house_200_132"},
    {"filename": "test_images/presentation_200_113.png", "shape": (113, 200), "name": "presentation_200_113"},
    {"filename": "test_images/selfie_200_133.png", "shape": (133, 200), "name": "selfie_200_133"},
]

# survey image generation
PAIR_IMAGE_TEST_RESULTS_FILE = "./pair_image_results.txt"
REAL_IMAGE_TEST_RESULTS_FILE = "./real_image_test_results.txt"

# execution time and memory tests
TEST_IMAGES_CONFIG = {
    "same_image_different_resolutions": [
        {"filename": "test_images/time_tests/baboon_25_25.png", "shape": (25, 25), "name": "baboon_25_25"},
        {"filename": "test_images/time_tests/baboon_50_50.png", "shape": (50, 50), "name": "baboon_50_50"},
        {"filename": "test_images/time_tests/baboon_75_75.png", "shape": (75, 75), "name": "baboon_75_75"},
        {"filename": "test_images/time_tests/baboon_100_100.png", "shape": (100, 100), "name": "baboon_100_100"},
        {"filename": "test_images/time_tests/baboon_125_125.png", "shape": (125, 125), "name": "baboon_125_125"},
        {"filename": "test_images/time_tests/baboon_150_150.png", "shape": (150, 150), "name": "baboon_150_150"},
        {"filename": "test_images/time_tests/baboon_175_175.png", "shape": (175, 175), "name": "baboon_175_175"},
        {"filename": "test_images/time_tests/baboon_200_200.png", "shape": (200, 200), "name": "baboon_200_200"},
    ],
    "different_images_same_resolution": [
        {"filename": "test_images/time_tests/baboon_100_100.png", "shape": (100, 100), "name": "baboon_100_100"},
        {"filename": "test_images/time_tests/checkers_100_100.png", "shape": (100, 100), "name": "checkers_100_100"},
        {"filename": "test_images/time_tests/digits_100_100.png", "shape": (100, 100), "name": "digits_100_100"},
        {"filename": "test_images/time_tests/dog_100_100.png", "shape": (100, 100), "name": "dog_100_100"},
        {"filename": "test_images/time_tests/house_100_100.png", "shape": (100, 100), "name": "house_100_100"},
        {"filename": "test_images/time_tests/presentation_100_100.png", "shape": (100, 100), "name": "presentation_100_100"},
        {"filename": "test_images/time_tests/selfie_100_100.png", "shape": (100, 100), "name": "selfie_100_100"},
    ]
}