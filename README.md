# Bachelor of Science Thesis - Image Upscaling Algorithms

This project is part of my Bachelor of Science Thesis. It tackles topic of image upscaling algorithms from the canonical ones to using deep learning architectures. During its development I would try to test all the algorithms against each other.

## How to run
To install all dependencies and build the environment use this command
```sh
$ docker build -t imageupscaling:latest .
```
Then use this to run it
```sh
$ docker run -ti --rm -v $PWD:/app imageupscaling:latest bash
```