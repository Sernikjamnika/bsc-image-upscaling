import argparse
import sys

import pandas as pd


parser = argparse.ArgumentParser(description='Find max and mean of memory subreport')
parser.add_argument('-f', '--csv_file', help="CSV file")
args = parser.parse_args(sys.argv[1:])
df = pd.read_csv(args.csv_file)
scaler = df.iloc[0, 0]
maximum = df.iloc[:,1].max()
mean = df.iloc[:,2].mean()

with open("result_memory_new.csv", mode="a") as result_file:
    result_file.write(f"{list(df)}\n")
    result_file.write(f"{scaler},{maximum},{mean}\n")
