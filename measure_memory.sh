#!/bin/bash

for scaler in {0..6}
  do
  for image in {0..6}
    do
    echo $scaler $image
    for _ in {0..99}
      do
      python memory_tests.py -s $scaler -i $image
      done
    cat ./results_memory_${scaler}_${image}.csv
    python tmp.py -f ./results_memory_${scaler}_${image}.csv
    # rm ./results_memory_${scaler}_${image}.csv
    done
  done