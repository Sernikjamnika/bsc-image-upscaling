import gc

from consts import SCALERS_CONFIG, IMAGES_CONFIG, SCALING_DEGREE
from training.feature_loss import FeatureLoss


if __name__ == "__main__":
    for scaler_config in SCALERS_CONFIG:
        scaler_config = scaler_config
        scaler_class = scaler_config.pop("scaler_class")
        scaler = scaler_class(**scaler_config)
        normalized = scaler_config.get("normalized", False)
        for image_config in IMAGES_CONFIG:
            image_name = image_config["filename"]
            image_height, image_width = image_config["shape"]
            image = scaler.loader.load_image(image_name, normalized=normalized)
            upscaled_image = scaler.upscale(
                image, shape=(image_height * SCALING_DEGREE, image_width * SCALING_DEGREE)
            )
            scaler.saver.save(
                upscaled_image,
                f"./generated_images/{image_config.get('name', 'image')}_{scaler_config.get('filename', scaler_class)}.png",
                normalized=normalized,
                compress_level=0,
                optimize=False,
            )
            # free memory because of memory consuming DL architectures
            upscaled_image = None
            image = None
            gc.collect()
        # free memory because of memory consuming DL architectures
        scaler = None
        scaler_class = None
        gc.collect()