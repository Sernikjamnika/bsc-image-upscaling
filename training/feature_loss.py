from fastai.callbacks.hooks import hook_outputs
import torch
from torch import nn
import torch.nn.functional as F


class FeatureLoss(nn.Module):
    def __init__(self, model_features, layer_ids, layer_weights):
        super().__init__()
        self.model_features = model_features
        self.loss_features = [self.model_features[i] for i in layer_ids]
        self.hooks = hook_outputs(self.loss_features, detach=False)
        self.layer_weights = layer_weights
        self.gram_coefficient = 5000

    def create_features(self, x, clone=False):
        self.model_features(x)
        return [output.clone() if clone else output for output in self.hooks.stored]

    def forward(self, input, target):
        out_features = self.create_features(target, clone=True)
        in_features = self.create_features(input)
        self.feat_losses = [F.l1_loss(input, target)]
        self.feat_losses += [
            F.l1_loss(in_feature, out_feature) * w
            for in_feature, out_feature, w in zip(
                in_features, out_features, self.layer_weights
            )
        ]
        self.feat_losses += [
            F.l1_loss(self.gram_matrix(in_feature), self.gram_matrix(out_feature)) * w
            for in_feature, out_feature, w in zip(
                in_features, out_features, self.layer_weights
            )
        ]
        return sum(self.feat_losses)

    @staticmethod
    def gram_matrix(x):
        number, channels, height, width = x.size()
        x = x.view(number, channels, -1)
        return torch.matmul(x, x.transpose(1, 2)) / (channels * height * width)
