
upy_scaling.tests
=====================
Moduł przystosowany do ewaluacja algorytmów wykorzystywanych przez `upy_scaling`.

.. automodule:: upy_scaling.tests.base_tester
    :members:
.. automodule:: upy_scaling.tests.execution_time_tester
    :members:
.. automodule:: upy_scaling.tests.memory_usage_tester
    :members:
.. automodule:: upy_scaling.tests.survey_image_generator
    :members:
.. automodule:: upy_scaling.tests.report_generators.base_report_generator
    :members:
.. automodule:: upy_scaling.tests.report_generators.memory_report_generator
    :members:


