Konfiguracja pakietu upy_scaling
================================

Plik konfiguracyjny `config.py` posiada w sobie zmienne umożliwiające wybór algorytmów 
skalowania i zdjęć, na których mają zostać użyte.

* `SCALING_DEGREE` - liczba całkowita określająca stopień powiększenia

* `SCALERS_CONFIG` - lista słowników z konfiguracją algorytmów skalujących 
    - `scaler_class` - klasa algorytmu skalującego, np. `TorchDLScaler`
    - `path` - ścieżka do folderu z wagami modelu (opcjonalnie)
    - `filename` - nazwa pliku z wagami w folderze pod ścieżka `path` (opcjonalnie)
    - `model` - klasa modelu (opcjonalnie)
    - `normalized` - wartość logiczna mówiąca o tym, czy zdjęcie przed przekazaniem go do modelu ma zostać znormalizowane poprzez podzielenie wartości intensywności pikseli przez maksymalną wartość intensywności dla modelu RGB, tj. 255 (domyślna wartość `False`)

* `IMAGES_CONFIG` - lista słowników z konfiguracją zdjęć do powiększenia
    - `filename` - ścieżka do zdjęcia
    - `shape` - wymiary zdjęcia przed powięszenie w formie pary :math:`(wysokość, szerokość)`
    - `name` - nazwa, z której tworzona jest nazwa pliku z powiększonym zdjęciem

Dla testów zostały zdefiniowane dodatkowe zmienne

* `PAIR_IMAGE_TEST_RESULTS_FILE` - plik z odpowiedziami do testów jakościowych pomiędzy parami (które zdjęcie jest powiększone, a które oryginalne)

* `REAL_IMAGE_TEST_RESULTS_FILE` - plik z odpowiedziami do testów jakościowych na siatce ze wszystkimi zdjęciami powiększonymi (które zdjęcie zostało powiększone jakim algorytmem)

* `TEST_IMAGES_CONFIG` - konfiguracja zdjęć przeznaczonych do testów na czas wykonania i zużycie pamięci
    - `same_image_different_resolutions` - do testów dla tego samego zdjęcia i różnych rozdzielczości
        + `filename` - ścieżka do zdjęcia
        + `shape` - wymiary zdjęcia przed powięszenie w formie pary :math:`(wysokość, szerokość)`
        + `name` - nazwa, z której tworzona jest nazwa pliku z powiększonym zdjęciem
    - `different_images_same_resolution` - do testów na różnych zdjęciach, a tej samej rozdzielczości
        + `filename` - ścieżka do zdjęcia
        + `shape` - wymiary zdjęcia przed powięszenie w formie pary :math:`(wysokość, szerokość)`
        + `name` - nazwa, z której tworzona jest nazwa pliku z powiększonym zdjęciem