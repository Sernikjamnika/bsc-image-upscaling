upy_scaling.base
===================
Jest to bazowy moduł, który ma być podstawą do rozszerzania modułu upy_scaling
o następne technologie, architektury głęboko uczone oraz klasyczne algorytmy rozwiązujące
problem interpolacji pikseli.

.. automodule:: upy_scaling.base.loader
    :members:
.. automodule:: upy_scaling.base.saver
    :members:
.. automodule:: upy_scaling.base.scaler
    :members: