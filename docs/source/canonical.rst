upy_scaling.canonical
=====================
Moduł rozszerzający `upy_scaling.base`, udostępniający algorymty klasyczne.

.. automodule:: upy_scaling.canonical.nearest_neighbours
    :members:
.. automodule:: upy_scaling.canonical.bilinear
    :members:
.. automodule:: upy_scaling.canonical.lanczos
    :members: