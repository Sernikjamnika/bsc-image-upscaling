.. upy_scaling documentation master file, created by
   sphinx-quickstart on Tue Jan  7 00:26:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witaj w dokumentacji upy_scaling!
=======================================

Pakiet `upy_scaling` umożliwia wykorzystanie algorytmów klasycznych i głeboko uczonych
do powiększania zdjęć. Umożliwia również przeprowadzenie testów na czas wykonania, zużytą
pamięć oraz stworzyć zdjęcia do testów jakościowych.

.. toctree::
   :maxdepth: 2
   :caption: Wykorzystanie pakietu:
   
   installation.rst
   config.rst

.. toctree::
   :maxdepth: 2
   :caption: Moduly pakietu `upy_scaling`:

   base.rst
   canonical.rst
   dl.rst
   tests.rst


Indeksy i tabele
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
