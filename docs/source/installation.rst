Instalacja i wykorzystanie
==========================

Aby skorzystać z plików wykonywalnych najwygodniejsze będzie skorzystanie z platformy docker.
Do zbudowania kontenerów służy poniższa komenda, która uruchamiana jest z poziomu pliku `docker-compose.yml`.

.. code-block:: bash

    $ docker build -t imageupscaling:latest .

Po zbudowaniu środowiska użytkownik może skorzystać z kilku bazowych komend. 

Do wejścia do środowiska służy komenda

.. code-block:: bash

    $ docker run -ti --rm -v $PWD:/app imageupscaling:latest bash

Powiększanie zdjęć zgodnie z konfiguracją w pliku `const.py`

.. code-block:: bash

    $ docker run -ti --rm -v $PWD:/app imageupscaling:latest python main.py

Uruchomienie testów czasu wykonania algorytmu zgodnie z konfiguracją w pliku `const.py`

.. code-block:: bash

    $ docker run -ti --rm -v $PWD:/app imageupscaling:latest python time_tests.py

Uruchomienie testów zużytej pamięci przez algorytmy zgodnie z konfiguracją w pliku `const.py`

.. code-block:: bash

    $ docker run -ti --rm -v $PWD:/app imageupscaling:latest python measure_memory.sh

Generowanie zdjęć do testów jakościowych

.. code-block:: bash

    $ docker run -ti --rm -v $PWD:/app imageupscaling:latest python generate_survey_tests.py
