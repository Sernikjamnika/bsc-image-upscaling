import os
from typing import List

from consts import IMAGES_CONFIG, REAL_IMAGE_TEST_RESULTS_FILE, PAIR_IMAGE_TEST_RESULTS_FILE
from upy_scaling.tests.survey_image_generator import generate_survey_image


def partition_list(images: List[str], partition_length: int) -> List[str]:
    for start_index in range(0, len(images), partition_length):
        yield images[start_index:start_index + partition_length]

def create_answer_file(file, answers, header=""):
    partition = [answer + "\n" for answer in answers]
    file.writelines([header+"\n"] + partition)

def survey_image_generator(
    input_directory,
    output_directory,
    results_filename,
    partition_length=8,
    scaling_factor=4,
    images_in_row=3
):
    images = os.listdir(input_directory)
    images.sort()
    images = [os.path.join(input_directory, image) for image in images if not image.startswith(".")]
    with open(results_filename, mode="w+") as file:
        file.flush()
        for image_config, partition in zip(IMAGES_CONFIG, partition_list(images, partition_length)):
            image_height, image_width = image_config["shape"]
            image_table, image_order = generate_survey_image(
                partition,
                (image_width * scaling_factor, image_height * scaling_factor),
                images_in_row=images_in_row
            )
            image_table.save(f"{output_directory}/{image_config['name']}.png")
            create_answer_file(file, image_order, image_config['name'])

if __name__ == "__main__":
    survey_image_generator(
        "./gen_images",
        "./survey_images/real_image_tables",
        REAL_IMAGE_TEST_RESULTS_FILE
    )
    survey_image_generator(
        "./survey_images/pair_image",
        "./survey_images/pair_image_tables",
        PAIR_IMAGE_TEST_RESULTS_FILE,
        partition_length=2,
        images_in_row=2
    )
    